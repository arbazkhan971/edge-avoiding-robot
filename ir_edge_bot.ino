#define m1p1 4
#define m1p2 5
#define m2p1 6
#define m2p2 7
#define vm   13
#define pwm1 3
#define pwm2 9
//#define trig 10
//#define echo 11

#define IR1_Limit 710
#define IR2_Limit 890
#define IR3_Limit 770
#define IR4_Limit 650

#define IR1 A1
#define IR2 A2
#define IR3 A3
#define IR4 A4

#define IR1_5 8
#define IR2_5 878 // 5v used
#define IR3_5 10
#define IR4_5 121 //5v male used
#define IR1_0 100 // GND pin used
#define IR2_0 1001 // GND male pin used
#define IR3_0 11
#define IR4_0 12

//#define vb   8
long dur;
int dist;
char a;
/*char f;
char r;
char l;
*/
#define pwm_default 0
#define pwm_val1 230
#define pwm_val2 250


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

/*
  pinMode(IR1, INPUT); 
  pinMode(IR2, INPUT);
  pinMode(IR3, INPUT);
  pinMode(IR4, INPUT);
*/

pinMode(m1p1,OUTPUT);
pinMode(m1p2,OUTPUT);
pinMode(m2p1,OUTPUT);
pinMode(m2p2,OUTPUT);
pinMode(pwm1,OUTPUT);
pinMode(pwm2,OUTPUT);
pinMode(vm,OUTPUT);

digitalWrite(vm,HIGH);
//pinMode(vb,OUTPUT);
//pinMode(trig,OUTPUT);
//pinMode(echo,INPUT);
analogWrite(pwm1,pwm_val1);
analogWrite(pwm2,pwm_val2);

//5V and Ground pins for ir sensor
pinMode(IR1_5,OUTPUT);//5 v for 1
digitalWrite(IR1_5,HIGH);

pinMode(IR2_5,OUTPUT);
digitalWrite(IR2_5,HIGH);

pinMode(IR3_5,OUTPUT);
digitalWrite(IR3_5,HIGH);

pinMode(IR4_5,OUTPUT);
digitalWrite(IR4_5,HIGH);

pinMode(IR1_0,OUTPUT);
digitalWrite(IR1_0,LOW);

pinMode(IR2_0,OUTPUT);
digitalWrite(IR2_0,LOW);

pinMode(IR3_0,OUTPUT);
digitalWrite(IR3_0,LOW);

pinMode(IR4_0,OUTPUT);
digitalWrite(IR4_0,LOW);

}

void forward() {
  digitalWrite(m1p1,LOW);
  digitalWrite(m1p2,HIGH);
  digitalWrite(m2p1,LOW);
  digitalWrite(m2p2,HIGH);
  delay(100);
  }

void right() {
  digitalWrite(m1p1,HIGH);  
  digitalWrite(m1p2,LOW);
  digitalWrite(m2p1,LOW);
  digitalWrite(m2p2,HIGH);
  delay(100);
  }

void left() {
  digitalWrite(m1p1,LOW);
  digitalWrite(m1p2,HIGH);
  digitalWrite(m2p1,HIGH);
  digitalWrite(m2p2,LOW);
  delay(100);
}

void halt(){
  digitalWrite(m1p1,HIGH);
  digitalWrite(m1p2,HIGH);
  digitalWrite(m2p1,HIGH);
  digitalWrite(m2p2,HIGH);
  delay(100);       
}

void reset() {
  analogWrite(pwm1,pwm_val1);
  analogWrite(pwm2,pwm_val2);
}

void reverse(){
  digitalWrite(m1p1,HIGH);
  digitalWrite(m1p2,LOW);
  digitalWrite(m2p1,HIGH);
  digitalWrite(m2p2,LOW);
  delay(100);
}



void loop() {
  // put your main code here, to run repeatedly:
 /*

// ULTRA SONIC SECTION
  digitalWrite(vm,HIGH);
  digitalWrite(trig,LOW);
  delayMicroseconds(2);
  digitalWrite(trig,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig,LOW);
  dur=pulseIn(echo,HIGH);
dist=dur*0.034/2;
*/

//IR SECTION
int detectionIR1 = 0;
int detectionIR2 = 0;
int detectionIR3 = 0;
int detectionIR4 = 0;
  
int readIR1 = analogRead(IR1);
int readIR2 = analogRead(IR2);
int readIR3 = analogRead(IR3);
int readIR4 = analogRead(IR4);

//////////Serial.print(analogRead(IR1));
//Serial.print("\t");

//Serial.print(analogRead(IR2));
//Serial.print("\t");
//Serial.print(analogRead(IR3));
//Serial.print("\t");
//Serial.print(analogRead(IR4));
//Serial.print("\n\n");



/*if it is not reflected back, then bot is at edge */
if(readIR1 >IR1_Limit) detectionIR1 = 1;
if(readIR2 >IR2_Limit) detectionIR2 = 1;
if(readIR3 >IR3_Limit) detectionIR3 = 1;
if(readIR4 >IR4_Limit) detectionIR4 = 1;



  /* make if else ladder according to state of
  detection variables */

if(detectionIR1 == 0 && detectionIR2 == 0 && detectionIR3 == 0 && detectionIR4 == 0)
{
  a=Serial.read();
   switch(a)
   {
    case'f': 
   forward();
    ////Serial.print("forward");
   break;
   
   case 'r': 
    right();
    ////Serial.print("right");
    break;
   
  case 'l': 
    left();
    ////Serial.print("left");
    break;
   
   case 'b':
    reverse();
    ////Serial.print("reverse");
    break;


   case 'h':
    halt();
    reset();
    ////Serial.print("halt");
    break;
   
    

  }
}
else if(detectionIR1 == 0 && detectionIR2 == 0 && detectionIR3 == 0 && detectionIR4 == 1)
{
  halt();
  left();
  forward();
  delay(100);
  halt();
  reset();
}
else if(detectionIR1 == 0 && detectionIR2 == 0 && detectionIR3 == 1 && detectionIR4 == 0)
{
  halt();
  right();
  forward();
  delay(100);
  halt();
  reset();
}
else if(detectionIR1 == 0 && detectionIR2 == 0 && detectionIR3 == 1 && detectionIR4 == 1)
{
halt();
forward();
delay(100);
halt();
reset();
}
else if(detectionIR1 == 0 && detectionIR2 == 1 && detectionIR3 == 0 && detectionIR4 == 0)
{
  halt();
  reverse();
  delay(100);
  halt();
  reset();
}
else if(detectionIR1 == 0 && detectionIR2 == 1 && detectionIR3 == 0 && detectionIR4 == 1)
{
  halt();
  left();
  delay(100);
  halt();
  reset();
}

//IMPOSSIBLE CONDITION
//else if(detectionIR1 == 0 && detectionIR2 == 1 && detectionIR3 == 1 && detectionIR4 == 0) {}

else if(detectionIR1 == 0 && detectionIR2 == 1 && detectionIR3 == 1 && detectionIR4 == 1)
{
  halt();
  left();
  delay(100);
  halt();
  reset();
}

else if(detectionIR1 == 1 && detectionIR2 == 0 && detectionIR3 == 0 && detectionIR4 == 0)
{
  halt();
  reverse();
  delay(100);
  halt();
  reset();
}

//IMPOSSIBLE CONDITION
//else if(detectionIR1 == 1 && detectionIR2 == 0 && detectionIR3 == 0 && detectionIR4 == 1)

else if(detectionIR1 == 1 && detectionIR2 == 0 && detectionIR3 == 1 && detectionIR4 == 0)
{
  halt();
  right();
  delay(100);
  halt();
  reset();
}

else if(detectionIR1 == 1 && detectionIR2 == 0 && detectionIR3 == 1 && detectionIR4 == 1)
{
  halt();
  right();
  delay(100);
  halt();
  reset();
}

else if(detectionIR1 == 1 && detectionIR2 == 1 && detectionIR3 == 0 && detectionIR4 == 0)
{
  halt();
  reverse();
  delay(100);
  halt();
  reset();
}

else if(detectionIR1 == 1 && detectionIR2 == 1 && detectionIR3 == 0 && detectionIR4 == 1)
{
  halt();
  right();
  delay(100);
  halt();
  reset();
}

else if(detectionIR1 == 1 && detectionIR2 == 1 && detectionIR3 == 1 && detectionIR4 == 0)
{
  halt();
  left();
  delay(100);
  halt();
  reset();
}

//IMPOSSIBLE CONDITION
//else if(detectionIR1 == 1 && detectionIR2 == 1 && detectionIR3 == 1 && detectionIR4 == 1)



/*
//ULTRA SONIC
if(dist<12)
{
  a=Serial.read();
   switch(a)
   {
    case'f': 
   forward();
    //Serial.print("forward");
   break;
   
   case 'r': 
    right();
    //Serial.print("right");
    break;
   
  case 'l': 
    left();
    //Serial.print("left");
    break;
   
   case 'h':
    halt();
    reset();
    //Serial.print("halt");
    break;
   
}
}
else
{
  halt();
  right();
  delay(100);
  halt();
  reset();
}
*/





}

